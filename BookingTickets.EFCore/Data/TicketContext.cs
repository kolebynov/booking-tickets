﻿using System;
using BookingTickets.EFCore.Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookingTickets.EFCore.Data
{
    public class TicketContext : IdentityDbContext<User, Role, Guid>
    {
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TicketType> TicketTypes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        public TicketContext(DbContextOptions<TicketContext> options) : base(options)
        { }
    }
}