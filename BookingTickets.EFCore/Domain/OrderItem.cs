﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookingTickets.EFCore.Domain
{
    public class OrderItem : BaseEntity
    {
        [ForeignKey(nameof(Order))]
        public Guid OrderId { get; set; }
        [ForeignKey(nameof(Ticket))]
        public Guid TicketId { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }

        public Order Order { get; set; }
        public Ticket Ticket { get; set; }
    }
}
