﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookingTickets.EFCore.Domain
{
    public class Ticket : BaseEntity
    {
        public string Description { get; set; }
        [ForeignKey(nameof(TicketType))]
        public Guid TypeId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public DateTime EventTime { get; set; }

        public TicketType TicketType { get; set; }
    }
}