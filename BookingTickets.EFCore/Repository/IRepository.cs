﻿using BookingTickets.EFCore.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookingTickets.EFCore.Repository
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        IQueryable<TEntity> Entities { get; }

        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(Guid id);
        Task<TEntity> GetByIdAsync(Guid id);
    }
}
