﻿using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingTickets.Fake.Repository
{
    public class BaseFakeRepository<TEntity> : IRepository<TEntity>
        where TEntity : BaseEntity
    {
        public virtual List<TEntity> Items { get; set; } = new List<TEntity>();
        public IQueryable<TEntity> Entities => Items.AsQueryable();

        public Task DeleteAsync(Guid id)
        {
            return Task.Run(() => Items.RemoveAll(e => e.Id == id));
        }

        public Task<TEntity> GetByIdAsync(Guid id)
        {
            return Task.Run(() => Items.Find(e => e.Id == id));
        }

        public Task UpdateAsync(TEntity entity)
        {
            return Task.Run(() =>
            {
                int index = Items.FindIndex(e => e.Id == entity.Id);
                if (index > -1)
                {
                    Items[index] = entity;
                }
                else
                {
                    Items.Add(entity);
                }
            });
        }
    }
}
