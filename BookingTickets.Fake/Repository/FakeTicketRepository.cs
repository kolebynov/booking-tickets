﻿using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingTickets.Fake.Repository
{
    public class FakeTicketRepository : BaseFakeRepository<Ticket>
    {
        private readonly Lazy<List<Ticket>> _items;

        public override List<Ticket> Items => _items.Value;

        public FakeTicketRepository(IRepository<TicketType> ticketTypesRepository)
        {
            Random rnd = new Random(Environment.TickCount);
            int typesCount = ticketTypesRepository.Entities.Count();
            _items = new Lazy<List<Ticket>>(() => new List<Ticket>
            (
                Enumerable.Range(0, 150).Select(idx =>
                {
                    TicketType type = ticketTypesRepository.Entities.ElementAt(rnd.Next(0, typesCount));
                    return new Ticket
                    {
                        Id = Guid.NewGuid(),
                        Description = "Description " + idx,
                        EventTime = DateTime.Now,
                        Price = rnd.Next(1, 1000),
                        Quantity = rnd.Next(0, 10),
                        TicketType = type,
                        TypeId = type.Id
                    };
                })
            ), true);
        }
    }
}
