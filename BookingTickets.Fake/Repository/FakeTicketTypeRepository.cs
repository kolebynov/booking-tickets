﻿using BookingTickets.EFCore.Domain;
using System;
using System.Collections.Generic;

namespace BookingTickets.Fake.Repository
{
    public class FakeTicketTypeRepository : BaseFakeRepository<TicketType>
    {
        private static readonly List<TicketType> items = new List<TicketType>
        {
            new TicketType
            {
                Id = new Guid("{6BC9916E-60A8-4034-8CB4-D45D2D24644C}"),
                Name = "Кинотеатр"
            },
            new TicketType
            {
                Id = new Guid("{22CE26B6-AC31-41AA-9CA1-E6A65C0EE9ED}"),
                Name = "Аэропорт"
            },
            new TicketType
            {
                Id = new Guid("{096B9323-98D1-4934-B0D8-8F87E617C82C}"),
                Name = "ЖД"
            },
        };

        public override List<TicketType> Items => items;
    }
}
