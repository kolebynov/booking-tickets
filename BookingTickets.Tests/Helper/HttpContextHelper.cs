﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Moq;

namespace BookingTickets.Tests.Helper
{
    public static class HttpContextHelper
    {
        public static IHttpContextAccessor GetHttpContextAccessor(string requestPath = null, string requestQuery = null, string method = "GET")
        {
            Mock<IHttpContextAccessor> mock = new Mock<IHttpContextAccessor>();
            mock.SetupGet(m => m.HttpContext).Returns(GetHttpContext(requestPath, requestQuery, method));
            return mock.Object;
        }

        public static HttpContext GetHttpContext(string requestPath = null, string requestQuery = null, string method = "GET")
        {
            Mock<HttpContext> mock = new Mock<HttpContext>();
            mock.SetupGet(m => m.Request).Returns(GetRequest(requestPath, requestQuery, method));
            return mock.Object;
        }

        public static HttpRequest GetRequest(string requestPath = null, string requestQuery = null,
            string method = "GET")
        {
            Mock<HttpRequest> mock = new Mock<HttpRequest>();
            mock.SetupGet(m => m.Path).Returns(requestPath);
            mock.SetupGet(m => m.QueryString).Returns(new QueryString(requestQuery));
            mock.SetupGet(m => m.Method).Returns(method);
            return mock.Object;
        }
    }
}
