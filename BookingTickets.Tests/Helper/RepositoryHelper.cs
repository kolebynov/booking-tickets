﻿using System;
using System.Collections.Generic;
using System.Text;
using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using BookingTickets.Fake.Repository;

namespace BookingTickets.Tests.Helper
{
    public static class RepositoryHelper
    {
        public static BaseFakeRepository<TicketType> TicketTypeRepository { get; } = new BaseFakeRepository<TicketType>
        {
            Items =
            {
                new TicketType
                {
                    Id = new Guid("{6BC9916E-60A8-4034-8CB4-D45D2D24644C}"),
                    Name = "Кинотеатр"
                },
                new TicketType
                {
                    Id = new Guid("{22CE26B6-AC31-41AA-9CA1-E6A65C0EE9ED}"),
                    Name = "Аэропорт"
                },
                new TicketType
                {
                    Id = new Guid("{096B9323-98D1-4934-B0D8-8F87E617C82C}"),
                    Name = "ЖД"
                }
            }
        };

        public static BaseFakeRepository<Ticket> TicketRepository { get; } = new BaseFakeRepository<Ticket>
        {
            Items =
            {
                new Ticket
                {
                    Id = new Guid("{92E6A3D3-0D0A-433D-B8E3-FED2FD37469F}"),
                    Description = "Description 0",
                    EventTime = DateTime.Now,
                    Price = 1,
                    Quantity = 1,
                    TicketType = TicketTypeRepository.Items[0],
                    TypeId = TicketTypeRepository.Items[0].Id
                },
                new Ticket
                {
                    Id = new Guid("{BFF6583E-8C63-4899-BD20-973E555F45F0}"),
                    Description = "Description 1",
                    EventTime = DateTime.Now,
                    Price = 2,
                    Quantity = 2,
                    TicketType = TicketTypeRepository.Items[1],
                    TypeId = TicketTypeRepository.Items[1].Id
                },
                new Ticket
                {
                    Id = new Guid("{B1970AA0-587F-4FAB-8685-B8FA4CBF036E}"),
                    Description = "Description 2",
                    EventTime = DateTime.Now,
                    Price = 2,
                    Quantity = 2,
                    TicketType = TicketTypeRepository.Items[2],
                    TypeId = TicketTypeRepository.Items[2].Id
                },
                new Ticket
                {
                    Id = new Guid("{34F80426-4369-42C1-86E8-72ADA04C5A9D}"),
                    Description = "Description 3",
                    EventTime = DateTime.Now,
                    Price = 3,
                    Quantity = 3,
                    TicketType = TicketTypeRepository.Items[0],
                    TypeId = TicketTypeRepository.Items[0].Id
                },
                new Ticket
                {
                    Id = new Guid("{8A0302D2-0D41-43A1-A25B-0208650F64D2}"),
                    Description = "Description 4",
                    EventTime = DateTime.Now,
                    Price = 1,
                    Quantity = 0,
                    TicketType = TicketTypeRepository.Items[1],
                    TypeId = TicketTypeRepository.Items[1].Id
                }
            }
        };
    }
}
