﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BookingTickets.Converter;
using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using BookingTickets.Fake.Repository;
using BookingTickets.Model;
using BookingTickets.ModelFilter;
using BookingTickets.Provider;
using BookingTickets.Service;
using BookingTickets.Tests.Helper;
using BookingTickets.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BookingTickets.Tests.Service
{
    [TestClass]
    public class HomeViewModelServiceTests
    {
        /*[TestMethod]
        public void Can_Return_All_Data()
        {
            //arrange
            TicketViewModelService service = GetService();

            //act
            TicketsViewModel viewModel = service.GetViewModel(new TicketIndexModel
            {
                Count = "all"
            });

            //Assert
            Assert.AreEqual(RepositoryHelper.TicketRepository.Items.Count, viewModel.Tickets.Count());
            Assert.AreEqual(1, viewModel.PaginationData.CurrentPage);
            Assert.AreEqual(RepositoryHelper.TicketRepository.Items.Count, viewModel.PaginationData.TotalItems);
            Assert.AreEqual(RepositoryHelper.TicketRepository.Items.Count, viewModel.PaginationData.ItemsPerPage);
        }

        [TestMethod]
        public void Can_Return_2_Items()
        {
            //arrange
            TicketViewModelService service = GetService();

            //act
            TicketsViewModel viewModel = service.GetViewModel(new TicketIndexModel
            {
                Count = "2"
            });

            //Assert
            Assert.AreEqual(2, viewModel.Tickets.Count());
            Assert.AreEqual(RepositoryHelper.TicketRepository.Items.Count, viewModel.PaginationData.TotalItems);
            Assert.AreEqual(1, viewModel.PaginationData.CurrentPage);
            Assert.AreEqual(2, viewModel.PaginationData.ItemsPerPage);
            Assert.AreEqual(RepositoryHelper.TicketRepository.Items[0].Id, viewModel.Tickets.ElementAt(0).Id);
            Assert.AreEqual(RepositoryHelper.TicketRepository.Items[1].Id, viewModel.Tickets.ElementAt(1).Id);
        }

        [TestMethod]
        public void Can_Return_2_Items_On_2nd_Page()
        {
            //arrange
            TicketViewModelService service = GetService();

            //act
            TicketsViewModel viewModel = service.GetViewModel(new TicketIndexModel
            {
                Count = "2",
                Page = 2
            });

            //Assert
            Assert.AreEqual(2, viewModel.Tickets.Count());
            Assert.AreEqual(RepositoryHelper.TicketRepository.Items.Count, viewModel.PaginationData.TotalItems);
            Assert.AreEqual(2, viewModel.PaginationData.CurrentPage);
            Assert.AreEqual(2, viewModel.PaginationData.ItemsPerPage);
            Assert.AreEqual(RepositoryHelper.TicketRepository.Items[2].Id, viewModel.Tickets.ElementAt(0).Id);
            Assert.AreEqual(RepositoryHelper.TicketRepository.Items[3].Id, viewModel.Tickets.ElementAt(1).Id);
        }

        private TicketViewModelService GetService()
        {
            TicketConverter ticketConverter = new TicketConverter();
            TicketTypeConverter ticketTypeToViewModelConverter = new TicketTypeConverter();
            IHttpContextAccessor httpContextAccessor = HttpContextHelper.GetHttpContextAccessor();
            TicketProvider ticketProvider = new TicketProvider(RepositoryHelper.TicketRepository);
            ItemsPerPagesProvider itemsPerPagesProvider = new ItemsPerPagesProvider();
            TicketFilter ticketFilter = new TicketFilter();
            TicketIndexModelConverter homeIndexModelToTicketSearchModelConverter = 
                new TicketIndexModelConverter();
            return new TicketViewModelService(ticketConverter, new FakeTicketTypeRepository(),
                ticketTypeToViewModelConverter, httpContextAccessor, ticketProvider, itemsPerPagesProvider,
                ticketFilter, homeIndexModelToTicketSearchModelConverter);
        }*/
    }
}
