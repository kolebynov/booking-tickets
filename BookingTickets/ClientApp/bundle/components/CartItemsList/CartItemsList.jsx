﻿import React from "react";

class CartItemsList extends React.Component {
    constructor() {
        super();
        this.state = {
            items: []
        };
    }

    render() {
        return (
            <div>
                <table id="CartItems">
                    <thead>
                        <tr>
                            <th>Тип</th>
                            <th>Описание</th>
                            <th>Время</th>
                            <th>Количество</th>
                            <th>Сумма</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        );
    }
}

export default CartItemsList;