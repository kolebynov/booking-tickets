﻿import React from "react";

class TicketList extends React.Component {
    constructor() {
        super();
        this.state = {
            tickets: []
        };
    }

    componentDidMount() {

    }

    render() {
        return (
            <div id="TicketsContentContainer" className="collection">
                <div id="TicketsHeaderContainer" className="ticketColumns collection-item">
                    <div style={{ width: "50%" }} className="ticketHeaderColumn">Описание</div>
                    <div style={{ width: "25%" }} className="ticketHeaderColumn">Цена</div>
                    <div style={{ width: "25%" }} className="ticketHeaderColumn">Количество</div>
                </div>
                <div id="TicketsListContainer">
                    {this.state.tickets.map(ticket => this.renderTicket(ticket))}
                </div>
            </div>
        );
    }

    renderTicket(ticket) {
        const ticketContainerId = "TicketContainer-" + ticket.id;
        const ticketInfoContainerId = "TicketInfoContainer-" + ticket.id;
        const ticketActionsContainerId = "TicketActionsContainer-" + ticket.id;
        const quantity = ticket.quantity > 0 ? ticket.quantity : "Нет в наличии";

        return (
            <div id={ticketContainerId} className="collection-item">
                <div id={ticketInfoContainerId} className="ticketColumns">
                    <div style="width:50%">{ticket.description}</div>
                    <div style="width:25%">{ticket.price}</div>
                    <div style="width:25%">{quantity}</div>
                </div>
                <div id={ticketActionsContainerId}>
                        <a className="waves-effect waves-light btn" asp-controller="Ticket" asp-action="Edit" asp-route-id="@id">Просмотр</a>
                        <a className="waves-effect waves-light btn" asp-controller="Ticket" asp-action="Delete" asp-route-id="@id">Удалить</a>
                        
                    <input type="hidden" asp-for="Id" name="ticketId" />
                                <input type="hidden" name="returnUrl" value="@ViewContext.HttpContext.Request.PathAndQuery()" />
                            <input type="submit" className="waves-effect waves-light btn" value="В корзину" />
                </div>
            </div>
        );
    }
}

TicketList.propTypes = {
};

export default TicketList;