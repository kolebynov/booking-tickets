﻿import _ from "underscore";

let logoutButton = document.querySelector("#LogoutButton");
if (_.isElement(logoutButton)) {
    logoutButton.addEventListener("click", function() {
        let logoutForm = document.querySelector("#LogoutFrom");
        if (_.isElement(logoutForm)) {
            logoutForm.submit();
        }
    });
}