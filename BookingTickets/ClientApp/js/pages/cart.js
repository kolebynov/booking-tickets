﻿import _ from "underscore";
import Materialize from "materialize-css";

const updateDelay = 500;
const toastTimeout = 2000;
const controller = "Cart";
const action = "UpdateItems";
const successUpdatedMessage = "Изменения сохранены";
const errorUpdatedMessage = "Произошла ошибка во время сохранения";
const changesNotSaved = "Изменения не сохранены.";

function updateAmount(input) {
    const quantity = parseFloat(input.value);
    const amountElement =
        document.querySelector("#CartItemRow-" + input.dataset.ticketid + " .ticket-amount");
    if (!Number.isNaN(quantity) && amountElement) {
        const amount = parseFloat(input.dataset.price) * quantity;
        amountElement.innerHTML = amount;
    }
}

function getQuantityInputs() {
    return [...document.querySelectorAll("#CartItems .ticket-quantity")];
}

function getUpdateQuantityData() {
    return getQuantityInputs().map(input => ({
        ticketId: input.dataset.ticketid,
        quantity: parseInt(input.value)
    }));
}

const updateTicketsQuantityInCart = _.debounce(() => {
    if (validateInputs()) {
        const data = {
            items: getUpdateQuantityData()
        };
        fetch(`/${controller}/${action}`, {
            method: "POST",
            body: JSON.stringify(data),
            credentials: "include",
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(updateQuantityResponseHandler)
        .catch(updateQuantityResponseHandler);
    }
    else {
        Materialize.toast(changesNotSaved, toastTimeout);
    }
}, updateDelay);

function updateQuantityResponseHandler(response) {
    debugger;
    let messageText;
    if (response.ok) {
        messageText = successUpdatedMessage;
    }
    else {
        messageText = errorUpdatedMessage;
    }
    Materialize.toast(messageText, toastTimeout);
}

function validateInputs() {
    let inputs = getQuantityInputs();
    return inputs.every(input => {
        let val = parseInt(input.value);
        return val >= parseInt(input.dataset.valRangeMin) && val <= parseInt(input.dataset.valRangeMax);
    });
}

const inputs = getQuantityInputs();
inputs.forEach(input => {
    input.addEventListener("blur", () => {
        updateAmount(input);
        updateTicketsQuantityInCart();
    });
});