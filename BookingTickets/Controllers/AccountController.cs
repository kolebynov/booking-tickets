﻿using BookingTickets.Model;
using BookingTickets.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BookingTickets.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IViewModelsService _viewModelsService;

        public AccountController(IUserService userService, IViewModelsService viewModelsService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _viewModelsService = viewModelsService ?? throw new ArgumentNullException(nameof(viewModelsService));
        }

        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginData loginData, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                UserResult loginResult = await _userService.LoginAsync(loginData);
                if (loginResult.Success)
                {
                    return Redirect(returnUrl ?? "/");
                }

                ModelState.AddModelError("Email", "Неверный логин или пароль");
            }
            return View(_viewModelsService.GetLoginViewModel(loginData));
        }

        [AllowAnonymous]
        public IActionResult Registration()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registration(RegisterUserData registerData)
        {
            if (ModelState.IsValid)
            {
                UserResult registerResult = await _userService.RegisterNewUserAsync(registerData);
                if (registerResult.Success)
                {
                    return RedirectToAction(nameof(Login));
                }

                foreach (UserError error in registerResult.Errors)
                    ModelState.AddModelError("", error.Message);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _userService.LogoutAsync();
            return RedirectToAction(nameof(Login));
        }
    }
}