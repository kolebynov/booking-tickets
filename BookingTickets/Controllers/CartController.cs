﻿using BookingTickets.Model;
using BookingTickets.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BookingTickets.Controllers
{
    public class CartController : Controller
    {
        private readonly ICartService _cartService;
        private readonly IViewModelsService _viewModelsService;

        public CartController(ICartService cartService, IViewModelsService viewModelsService)
        {
            _cartService = cartService ?? throw new ArgumentNullException(nameof(cartService));
            _viewModelsService = viewModelsService ?? throw new ArgumentNullException(nameof(viewModelsService));
        }

        public IActionResult Index(string returnUrl)
        {
            return View(_viewModelsService.GetCartViewModel(returnUrl));
        }

        [HttpPost]
        //TODO: Заменить на другое действие.
        public async Task<IActionResult> UpdateItems([FromBody] UpdateCartItemsModel updateCartItemsModel)
        {
            if (ModelState.IsValid)
            {
                foreach (CartItem cartItem in updateCartItemsModel.Items)
                {
                    await _cartService.UpdateItemAsynс(cartItem);
                }

                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpPost]
        public async Task<IActionResult> AddItem(Guid ticketId, string returnUrl)
        {
            await _cartService.AddItemAsync(new CartItem(ticketId, 1));
            return Redirect(returnUrl ?? "/");
        }

        [HttpPost]
        public async Task<IActionResult> RemoveItem(Guid ticketId, string returnUrl)
        {
            await _cartService.RemoveItemAsync(ticketId);
            return RedirectToAction(nameof(Index), new { returnUrl });
        }
    }
}