﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.Service;
using Microsoft.AspNetCore.Mvc;

namespace BookingTickets.Controllers
{
    public class OrderController : Controller
    {
        private readonly IViewModelsService _viewModelsService;
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService, IViewModelsService viewModelsService)
        {
            _orderService = orderService ?? throw new ArgumentNullException(nameof(orderService));
            _viewModelsService = viewModelsService 
                ?? throw new ArgumentNullException(nameof(viewModelsService));
        }

        [ActionName("Checkout")]
        [HttpGet]
        public IActionResult CheckoutPage()
        {
            return View(_viewModelsService.GetOrderCheckoutViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Checkout()
        {
            await _orderService.CreateOrderAsync();
            return RedirectToAction(nameof(Complete));
        }

        public IActionResult Complete()
        {
            return View();
        }
    }
}