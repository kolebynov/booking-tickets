﻿using BookingTickets.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BookingTickets.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IViewModelsService _viewModelsService;

        public ProfileController(IViewModelsService viewModelsService)
        {
            _viewModelsService = viewModelsService ?? throw new ArgumentNullException(nameof(viewModelsService));
        }

        public async Task<IActionResult> Index()
        {
            return View(await _viewModelsService.GetProfileViewModelAsync());
        }
    }
}