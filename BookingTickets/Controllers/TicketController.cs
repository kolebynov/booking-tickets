﻿using BookingTickets.Converter;
using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using BookingTickets.Service;
using BookingTickets.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using BookingTickets.Model;
using Microsoft.AspNetCore.Authorization;

namespace BookingTickets.Controllers
{
    public class TicketController : Controller
    {
        private readonly IRepository<Ticket> _repository;
        private readonly IViewModelsService _viewModelsService;
        private readonly TicketConverter _ticketConverter;

        public TicketController(
            IRepository<Ticket> repository,
            IViewModelsService viewModelsService,
            TicketConverter ticketConverter
        )
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _viewModelsService = viewModelsService 
                ?? throw new ArgumentNullException(nameof(viewModelsService));
            _ticketConverter = ticketConverter 
                ?? throw new ArgumentNullException(nameof(ticketConverter));
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index(TicketIndexModel ticketIndexModel)
        {
            return View(await _viewModelsService.GetTicketsViewModelAsync(ticketIndexModel));
        }

        [AllowAnonymous]
        public async Task<ActionResult> Edit(Guid id)
        {
            Ticket ticket = await _repository.GetByIdAsync(id);
            return View(await _viewModelsService.GetTicketEditViewModelAsync(ticket));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleConstants.ADMIN_ROLE_NAME)]
        public async Task<ActionResult> Edit(Ticket ticket)
        {
            await _repository.UpdateAsync(ticket);
            return RedirectToAction("Index");
        }

        [ActionName("Delete")]
        [Authorize(Roles = RoleConstants.ADMIN_ROLE_NAME)]
        public async Task<ActionResult> DeletePage(Guid id)
        {
            Ticket ticket = await _repository.GetByIdAsync(id);
            return View(_ticketConverter.ConvertToModel(ticket));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleConstants.ADMIN_ROLE_NAME)]
        public async Task<ActionResult> Delete(Guid id)
        {
            await _repository.DeleteAsync(id);
            return RedirectToAction("Index");
        }
    }
}