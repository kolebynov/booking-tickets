﻿using BookingTickets.Model;
using BookingTickets.Provider;
using BookingTickets.Service;
using System;
using System.Linq;

namespace BookingTickets.Converter
{
    public class CartItemConverter
    {
        private readonly ITicketProvider _ticketProvider;
        private readonly TicketConverter _ticketConverter;
        private readonly ITicketAmountCalculator _ticketAmountCalculator;

        public CartItemConverter(
            ITicketProvider ticketProvider,
            TicketConverter ticketConverter, 
            ITicketAmountCalculator ticketAmountCalculator
        )
        {
            _ticketProvider = ticketProvider ?? throw new ArgumentNullException(nameof(ticketProvider));
            _ticketConverter = ticketConverter 
                ?? throw new ArgumentNullException(nameof(ticketConverter));
            _ticketAmountCalculator = ticketAmountCalculator 
                ?? throw new ArgumentNullException(nameof(ticketAmountCalculator));
        }

        public virtual CartItemModel ConvertToModel(CartItem cartItem)
        {
            TicketModel ticket = _ticketConverter.ConvertToModel(_ticketProvider.Get()
                .FirstOrDefault(t => t.Id == cartItem.TicketId));
            return cartItem == null
                ? null
                : new CartItemModel
                {
                    Ticket = ticket,
                    Quantity = cartItem.Quantity,
                    Amount = _ticketAmountCalculator.Calculate(ticket, cartItem.Quantity)
                };
        }
    }
}
