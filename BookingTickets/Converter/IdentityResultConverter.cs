﻿using BookingTickets.Model;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace BookingTickets.Converter
{
    public class IdentityResultConverter
    {
        public virtual UserResult ConvertToUserResult(IdentityResult src) =>
            new UserResult
            {
                Success = src.Succeeded,
                Errors = src.Errors.Select(e => new UserError
                {
                    Code = e.Code,
                    Message = e.Description
                })
            };
    }
}
