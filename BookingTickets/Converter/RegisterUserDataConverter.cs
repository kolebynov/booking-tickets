﻿using BookingTickets.EFCore.Domain;
using BookingTickets.Model;

namespace BookingTickets.Converter
{
    public class RegisterUserDataConverter
    {
        public virtual User ConvertToUser(RegisterUserData src) =>
           src == null ? null : new User
           {
               Email = src.Email,
               UserName = src.UserName,
               FirstName = src.FirstName,
               Name = src.Name
           };
    }
}
