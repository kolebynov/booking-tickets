﻿using BookingTickets.Model;
using Microsoft.AspNetCore.Identity;

namespace BookingTickets.Converter
{
    public class SignInResultConverter
    {
        public virtual UserResult ConvertToUserResult(SignInResult src) =>
            new UserResult
            {
                Success = src.Succeeded,
                Errors = new UserError[0]
            };
    }
}
