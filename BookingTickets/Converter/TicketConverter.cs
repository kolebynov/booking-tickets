﻿using System;
using BookingTickets.EFCore.Domain;
using BookingTickets.Model;
using BookingTickets.ViewModel;

namespace BookingTickets.Converter
{
    public class TicketConverter
    {
        public virtual TicketModel ConvertToModel(Ticket src) =>
            src == null ? null : new TicketModel
            {
                Description = src.Description,
                EventTime = src.EventTime,
                Id = src.Id,
                Price = src.Price,
                Quantity = src.Quantity,
                Type = new NameValuePair<Guid>(src.TicketType?.Name, src.TypeId)
            };
    }
}
