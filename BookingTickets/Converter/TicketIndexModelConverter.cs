﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.Model;

namespace BookingTickets.Converter
{
    public class TicketIndexModelConverter
    {
        public virtual TicketSearchModel ConvertToTicketSearchModel(TicketIndexModel src) =>
            src == null
                ? null
                : new TicketSearchModel
                {
                    EventTimeFor = src.EventTimeFor,
                    Description = src.Description,
                    TypeId = src.TypeId,
                    PriceFor = src.PriceFor,
                    QuantityFor = src.QuantityFor,
                    QuantityTo = src.QuantityTo,
                    PriceTo = src.PriceTo,
                    EventTimeTo = src.EventTimeTo
                };
    }
}
