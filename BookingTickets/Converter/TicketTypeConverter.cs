﻿using BookingTickets.EFCore.Domain;
using BookingTickets.Model;
using BookingTickets.ViewModel;

namespace BookingTickets.Converter
{
    public class TicketTypeConverter
    {
        public virtual TicketTypeModel ConvertToModel(TicketType src) =>
            src == null ? null : new TicketTypeModel
            {
                Id = src.Id,
                Name = src.Name
            };
    }
}
