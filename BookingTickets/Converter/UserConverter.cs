﻿using BookingTickets.EFCore.Domain;
using BookingTickets.Model;

namespace BookingTickets.Converter
{
    public class UserConverter
    {
        public virtual ProfileInfoModel ConvertToProfileInfo(User src) =>
            src == null
                ? null
                : new ProfileInfoModel
                {
                    FirstName = src.FirstName,
                    Email = src.Email,
                    UserName = src.UserName,
                    Name = src.Name
                };
    }
}
