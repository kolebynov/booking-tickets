﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingTickets.Extension
{
    public static class SelectListExtensions
    {
        public static IEnumerable<SelectListItem> ConvertToSelectListItems<TElem, TValue>(
            this IEnumerable<TElem> collection,
            TValue selectedValue,
            Func<TElem, TValue> valueSelector,
            Func<TElem, string> textSelector,
            Func<TElem, bool> selectedPredicate = null,
            Func<TElem, bool> disabledPredicate = null
        )
        {
            if (selectedPredicate == null)
            {
                selectedPredicate = (elem) => (valueSelector(elem)?.Equals(selectedValue)).GetValueOrDefault();
            }
            return collection.Select(elem => new SelectListItem
            {
                Value = valueSelector(elem)?.ToString(),
                Text = textSelector(elem),
                Selected = selectedPredicate(elem),
                Disabled = (disabledPredicate?.Invoke(elem)).GetValueOrDefault()
            });
        }
    }
}
