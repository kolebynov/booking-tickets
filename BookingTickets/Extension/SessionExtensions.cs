﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace BookingTickets.Extension
{
    public static class SessionExtensions
    {
        public static void SetJson<T>(this ISession session, string key, T data)
        {
            session.SetString(key, JsonConvert.SerializeObject(data));
        }

        public static T GetJson<T>(this ISession session, string key)
        {
            string data = session.GetString(key);
            return data == null ? default(T) : JsonConvert.DeserializeObject<T>(data); 
        }
    }
}
