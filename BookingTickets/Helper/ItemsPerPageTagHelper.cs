﻿using BookingTickets.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;

namespace BookingTickets.Helper
{
    [HtmlTargetElement("div", Attributes = "items-per-pages")]
    public class ItemsPerPageLinkTagHelper : TagHelper
    {
        private readonly IUrlHelperFactory _urlHelperFactory;

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }
        public IEnumerable<NameValuePair<string>> ItemsPerPages { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string CountParamName { get; set; } = "count";
        public IDictionary<string, object> RouteValues { get; set; } = new Dictionary<string, object>();

        public ItemsPerPageLinkTagHelper(IUrlHelperFactory urlHelperFactory)
        {
            _urlHelperFactory = urlHelperFactory ?? throw new ArgumentNullException(nameof(urlHelperFactory));
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            IUrlHelper urlHelper = _urlHelperFactory.GetUrlHelper(ViewContext);
            TagBuilder tagBuilder = new TagBuilder("div");
            foreach (NameValuePair<string> itemsPerPage in ItemsPerPages)
            {
                TagBuilder linkTag = new TagBuilder("a");
                Dictionary<string, object> linkValues = new Dictionary<string, object>
                {
                    [CountParamName] = itemsPerPage.Value
                };
                foreach (var pair in RouteValues)
                {
                    linkValues.Add(pair.Key, pair.Value);
                }
                linkTag.Attributes["href"] = urlHelper.Action(Action, Controller, linkValues);
                linkTag.InnerHtml.Append(itemsPerPage.Name ?? itemsPerPage.Value);
                tagBuilder.InnerHtml.AppendHtml(linkTag);
            }
            output.Content.AppendHtml(tagBuilder);
        }
    }
}
