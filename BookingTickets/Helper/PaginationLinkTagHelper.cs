﻿using System;
using BookingTickets.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;

namespace BookingTickets.Helper
{
    [HtmlTargetElement("div", Attributes = "pagination-data")]
    public class PaginationLinkTagHelper : TagHelper
    {
        private readonly IUrlHelperFactory _urlHelperFactory;

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }
        public PaginationData PaginationData { get; set; }
        public string PageController { get; set; }
        public string PageAction { get; set; }
        public string PageParamName { get; set; } = "page";
        public string CountParamName { get; set; } = "count";
        public IDictionary<string, object> RouteValues { get; set; } = new Dictionary<string, object>();

        public PaginationLinkTagHelper(IUrlHelperFactory urlHelperFactory)
        {
            _urlHelperFactory = urlHelperFactory ?? throw new ArgumentNullException(nameof(urlHelperFactory));
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            IUrlHelper urlHelper = _urlHelperFactory.GetUrlHelper(ViewContext);
            TagBuilder tagBuilder = new TagBuilder("div");
            for (int i = 1; i <= PaginationData.PageCount; ++i)
            {
                TagBuilder linkTag = new TagBuilder("a");
                if (i != PaginationData.CurrentPage)
                {
                    Dictionary<string, object> linkValues = new Dictionary<string, object>
                    {
                        [PageParamName] = i,
                        [CountParamName] = PaginationData.ItemsPerPage
                    };
                    foreach (var pair in RouteValues)
                    {
                        linkValues.Add(pair.Key, pair.Value);
                    }
                    linkTag.Attributes["href"] = urlHelper.Action(PageAction, PageController, linkValues);
                }
                linkTag.InnerHtml.Append(i.ToString());
                tagBuilder.InnerHtml.AppendHtml(linkTag);
            }
            output.Content.AppendHtml(tagBuilder);
        }
    }
}
