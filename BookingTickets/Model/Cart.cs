﻿using BookingTickets.EFCore.Domain;
using BookingTickets.Extension;
using System;
using System.Collections.Generic;

namespace BookingTickets.Model
{
    public class Cart
    {
        private readonly List<CartItem> _cartItems = new List<CartItem>();

        public IReadOnlyList<CartItem> CartItems => _cartItems;

        public event EventHandler<CartChangedEventArgs> Changed;

        public virtual void AddItem(Guid ticketId, int quantity)
        {
            AddItem(new CartItem(ticketId, quantity));
        }

        public virtual void AddItem(CartItem cartItem)
        {
            cartItem.CheckArgumentNull();
            CartItem item = _cartItems.Find(t => t.TicketId == cartItem.TicketId);
            UpdateOrAddItem(new CartItem(cartItem.TicketId, cartItem.Quantity + (item?.Quantity).GetValueOrDefault()));
        }

        public virtual void UpdateItem(Guid ticketId, int quantity)
        {
            UpdateItem(new CartItem(ticketId, quantity));
        }

        public virtual void UpdateItem(CartItem cartItem)
        {
            UpdateOrAddItem(cartItem);
        }

        public virtual void RemoveItem(Guid ticketId)
        {
            int removedCount = _cartItems.RemoveAll(t => t.TicketId == ticketId);
            if (removedCount > 0)
            {
                OnChanged();
            }
        }

        public virtual void Clear()
        {
            bool needThrowChangeEvent = _cartItems.Count > 0;
            _cartItems.Clear();
            if (needThrowChangeEvent)
            {
                OnChanged();
            }
        }

        protected virtual void OnChanged() => Changed?.Invoke(this, new CartChangedEventArgs());

        private void UpdateOrAddItem(CartItem cartItem)
        {
            int index = _cartItems.FindIndex(t => t.TicketId == cartItem.TicketId);
            if (index > -1)
            {
                _cartItems[index] = cartItem;
            }
            else
            {
                _cartItems.Add(cartItem);
            }
            OnChanged();
        }
    }
}