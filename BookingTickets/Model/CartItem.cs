﻿using System;
using BookingTickets.EFCore.Domain;

namespace BookingTickets.Model
{
    public class CartItem
    { 
        public Guid TicketId { get; }
        public int Quantity { get; }

        public CartItem(Guid ticketId, int quantity)
        {
            TicketId = ticketId;
            Quantity = quantity;
        }
    }
}
