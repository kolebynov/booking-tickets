﻿using System;
using System.ComponentModel.DataAnnotations;
using BookingTickets.EFCore.Domain;
using BookingTickets.Validation;

namespace BookingTickets.Model
{
    public class CartItemModel
    {
        public TicketModel Ticket { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
    }
}