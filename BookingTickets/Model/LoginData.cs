﻿using System.ComponentModel.DataAnnotations;

namespace BookingTickets.Model
{
    public class LoginData
    {
        [Required, UIHint("email")]
        public string Email { get; set; }
        [Required, UIHint("password")]
        public string Password { get; set; }
    }
}
