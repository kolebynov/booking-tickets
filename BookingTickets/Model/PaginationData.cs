﻿using System;

namespace BookingTickets.Model
{
    public class PaginationData
    {
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalItems { get; set; }
        public int PageCount => ItemsPerPage > 0 ? (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage) : 0;
    }
}