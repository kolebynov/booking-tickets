﻿namespace BookingTickets.Model
{
    public class ProfileInfoModel
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
    }
}
