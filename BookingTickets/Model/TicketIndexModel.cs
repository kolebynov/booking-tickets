﻿using System;

namespace BookingTickets.Model
{
    public class TicketIndexModel
    {
        public Guid? TypeId { get; set; }
        public string Description { get; set; }
        public int? QuantityFor { get; set; }
        public int? QuantityTo { get; set; }
        public decimal? PriceFor { get; set; }
        public decimal? PriceTo { get; set; }
        public DateTime? EventTimeFor { get; set; }
        public DateTime? EventTimeTo { get; set; }
        public int Page { get; set; } = 1;
        public string Count { get; set; } = "0";
    }
}
