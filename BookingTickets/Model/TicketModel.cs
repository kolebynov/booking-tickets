﻿using System;

namespace BookingTickets.Model
{
    public class TicketModel
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public NameValuePair<Guid> Type { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public DateTime EventTime { get; set; }
    }
}
