﻿using System;

namespace BookingTickets.Model
{
    public class TicketSearchModel
    {
        public Guid? TypeId { get; set; }
        public string Description { get; set; }
        public int? QuantityFor { get; set; }
        public int? QuantityTo { get; set; }
        public decimal? PriceFor { get; set; }
        public decimal? PriceTo { get; set; }
        public DateTime? EventTimeFor { get; set; }
        public DateTime? EventTimeTo { get; set; }
    }
}
