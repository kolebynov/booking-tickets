﻿using System;

namespace BookingTickets.Model
{
    public class TicketTypeModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
