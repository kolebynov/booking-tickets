﻿using BookingTickets.Validation;

namespace BookingTickets.Model
{
    [UpdateCartItemsModelValidator]
    public class UpdateCartItemsModel
    {
        public CartItem[] Items { get; set; }
    }
}
