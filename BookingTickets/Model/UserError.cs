﻿namespace BookingTickets.Model
{
    public class UserError
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
