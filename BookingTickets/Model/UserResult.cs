﻿using System.Collections.Generic;

namespace BookingTickets.Model
{
    public class UserResult
    {
        public bool Success { get; set; }
        public IEnumerable<UserError> Errors { get; set; }
    }
}
