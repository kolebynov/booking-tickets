﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingTickets.ModelFilter
{
    public interface IFilter<TEntity, in TFilter>
    {
        IQueryable<TEntity> ApplyFilter(IQueryable<TEntity> query, TFilter filterModel);
    }
}
