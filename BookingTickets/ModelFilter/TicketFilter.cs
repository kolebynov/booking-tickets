﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.EFCore.Domain;
using BookingTickets.Model;

namespace BookingTickets.ModelFilter
{
    public class TicketFilter : IFilter<Ticket, TicketSearchModel>
    {
        public IQueryable<Ticket> ApplyFilter(IQueryable<Ticket> query, TicketSearchModel filterModel)
        {
            if (filterModel != null)
            {
                if (filterModel.EventTimeFor != null)
                {
                    query = query.Where(t => t.EventTime >= filterModel.EventTimeFor);
                }

                if (filterModel.EventTimeTo != null)
                {
                    query = query.Where(t => t.EventTime <= filterModel.EventTimeTo);
                }

                if (filterModel.PriceFor != null)
                {
                    query = query.Where(t => t.Price >= filterModel.PriceFor);
                }

                if (filterModel.PriceTo != null)
                {
                    query = query.Where(t => t.Price <= filterModel.PriceTo);
                }

                if (filterModel.QuantityFor != null)
                {
                    query = query.Where(t => t.Quantity >= filterModel.QuantityFor);
                }

                if (filterModel.QuantityTo != null)
                {
                    query = query.Where(t => t.Quantity <= filterModel.QuantityTo);
                }

                if (filterModel.Description != null)
                {
                    query = query.Where(t => t.Description.Contains(filterModel.Description));
                }

                if (filterModel.TypeId != null)
                {
                    query = query.Where(t => t.TypeId == filterModel.TypeId);
                }
            }

            return query;
        }
    }
}
