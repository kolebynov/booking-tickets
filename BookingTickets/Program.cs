﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using BookingTickets.EFCore.Data;
using BookingTickets.EFCore.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BookingTickets
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);
            using (var scope = host.Services.CreateScope())
            {
                IServiceProvider services = scope.ServiceProvider;
                TicketContext ticketContext = services.GetRequiredService<TicketContext>();
                ticketContext.Database.Migrate();
                IConfiguration configuration = services.GetRequiredService<IConfiguration>();
                if (!ticketContext.Roles.Any())
                {
                    InitRoles(services.GetRequiredService<RoleManager<Role>>());
                    InitAdminUsers(services.GetRequiredService<UserManager<User>>(), configuration);
                }
            }
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();

        private static void InitRoles(RoleManager<Role> roleManager)
        {
            Role adminRole = new Role
            {
                Name = RoleConstants.ADMIN_ROLE_NAME,
                NormalizedName = RoleConstants.ADMIN_ROLE_NAME.ToUpper()
            };
            Role userRole = new Role
            {
                Name = RoleConstants.USER_ROLE_NAME,
                NormalizedName = RoleConstants.USER_ROLE_NAME.ToUpper()
            };
            roleManager.CreateAsync(adminRole).Wait();
            roleManager.CreateAsync(userRole).Wait();
        }

        private static void InitAdminUsers(UserManager<User> userManager, IConfiguration configuration)
        {
            IEnumerable<IConfigurationSection> adminUsers = configuration.GetSection("AdminUsers").GetChildren();
            foreach (IConfigurationSection configurationSection in adminUsers)
            {
                User newUser = new User
                {
                    Email = configurationSection["email"],
                    UserName = configurationSection["login"]
                };
                userManager.CreateAsync(newUser, configurationSection["password"]).Wait();
                userManager.AddToRoleAsync(newUser, RoleConstants.USER_ROLE_NAME).Wait();
                userManager.AddToRoleAsync(newUser, RoleConstants.ADMIN_ROLE_NAME).Wait();
            }
        }
    }
}
