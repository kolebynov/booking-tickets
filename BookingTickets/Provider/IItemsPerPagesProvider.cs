﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.Model;

namespace BookingTickets.Provider
{
    public interface IItemsPerPagesProvider
    {
        IReadOnlyList<NameValuePair<string>> Get();
        string GetAllItemsParamName();
    }
}