﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.EFCore.Domain;

namespace BookingTickets.Provider
{
    public interface ITicketProvider
    {
        IQueryable<Ticket> Get();
    }
}
