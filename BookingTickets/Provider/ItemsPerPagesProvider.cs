﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.Model;

namespace BookingTickets.Provider
{
    public class ItemsPerPagesProvider : IItemsPerPagesProvider
    {
        private static readonly string ALL_PARAM_NAME = "all";
        private static readonly NameValuePair<string>[] ITEMS = 
        {
            new NameValuePair<string>("20"),
            new NameValuePair<string>("50"), new NameValuePair<string>("100"),
            new NameValuePair<string>("все", ALL_PARAM_NAME)
        };

        public IReadOnlyList<NameValuePair<string>> Get() => ITEMS;
            
        public string GetAllItemsParamName() => ALL_PARAM_NAME;
    }
}
