﻿using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using System;
using System.Linq;

namespace BookingTickets.Provider
{
    public class TicketProvider : ITicketProvider
    {
        private readonly IRepository<Ticket> _repository;

        public TicketProvider(IRepository<Ticket> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public IQueryable<Ticket> Get() =>
            _repository.Entities.Select(t => new Ticket
            {
                Id = t.Id,
                Description = t.Description,
                EventTime = t.EventTime,
                Price = t.Price,
                Quantity = t.Quantity,
                TicketType = t.TicketType,
                TypeId = t.TypeId
            });
    }
}
