﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingTickets
{
    public static class RoleConstants
    {
        public const string ADMIN_ROLE_NAME = "admin";
        public const string USER_ROLE_NAME = "user";
    }
}
