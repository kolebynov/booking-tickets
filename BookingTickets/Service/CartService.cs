﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.Converter;
using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using BookingTickets.Model;
using BookingTickets.Storage;

namespace BookingTickets.Service
{
    public class CartService : ICartService
    {
        private readonly CartItemConverter _cartItemConverter;
        private readonly IRepository<Ticket> _repository;
        private readonly ICartStorage _cartStorage;

        public CartService(
            CartItemConverter cartItemConverter, 
            IRepository<Ticket> repository,
            ICartStorage cartStorage
        )
        {
            _cartItemConverter = cartItemConverter 
                ?? throw new ArgumentNullException(nameof(cartItemConverter));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _cartStorage = cartStorage ?? throw new ArgumentNullException(nameof(cartStorage));
        }

        public async Task AddItemAsync(CartItem cartItem)
        {
            Ticket ticket = await _repository.GetByIdAsync(cartItem.TicketId);
            _cartStorage.Get().AddItem(ticket.Id, cartItem.Quantity);
            _cartStorage.Save();
        }

        public async Task RemoveItemAsync(Guid ticketId)
        {
            Ticket ticket = await _repository.GetByIdAsync(ticketId);
            _cartStorage.Get().RemoveItem(ticketId);
            _cartStorage.Save();
        }

        public IReadOnlyList<CartItemModel> GetItems() =>
            _cartStorage.Get().CartItems.Select(c => _cartItemConverter.ConvertToModel(c)).ToList();

        public void Clear()
        {
            _cartStorage.Get().Clear();
            _cartStorage.Save();
        }

        public async Task UpdateItemAsynс(CartItem cartItem)
        {
            Ticket ticket = await _repository.GetByIdAsync(cartItem.TicketId);
            _cartStorage.Get().UpdateItem(ticket.Id, cartItem.Quantity);
            _cartStorage.Save();
        }
    }
}