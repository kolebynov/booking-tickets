﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookingTickets.Model;

namespace BookingTickets.Service
{
    public interface ICartService
    {
        Task UpdateItemAsynс(CartItem cartItem);
        Task AddItemAsync(CartItem cartItem);
        Task RemoveItemAsync(Guid ticketId);
        IReadOnlyList<CartItemModel> GetItems();
        void Clear();
    }
}