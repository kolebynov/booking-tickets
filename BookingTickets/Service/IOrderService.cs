﻿using BookingTickets.EFCore.Domain;
using System.Threading.Tasks;

namespace BookingTickets.Service
{
    public interface IOrderService
    {
        Task<Order> CreateOrderAsync();
    }
}
