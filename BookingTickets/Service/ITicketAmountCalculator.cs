﻿using BookingTickets.Model;

namespace BookingTickets.Service
{
    public interface ITicketAmountCalculator
    {
        decimal Calculate(TicketModel ticket, int quantity);
    }
}
