﻿using System.Security.Claims;
using System.Threading.Tasks;
using BookingTickets.EFCore.Domain;
using BookingTickets.Model;

namespace BookingTickets.Service
{
    public interface IUserService
    {
        Task<UserResult> LoginAsync(LoginData loginData);
        Task LogoutAsync();
        Task<UserResult> RegisterNewUserAsync(RegisterUserData registerUserData);
        Task<bool> CurrentUserIsInRoleAsync(string role);
        Task<User> GetUserAsync(ClaimsPrincipal principal);
    }
}
