﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.EFCore.Domain;
using BookingTickets.Model;
using BookingTickets.ViewModel;

namespace BookingTickets.Service
{
    public interface IViewModelsService
    {
        CartViewModel GetCartViewModel(string returnUrl);
        LoginViewModel GetLoginViewModel(LoginData loginData);
        OrderCheckoutViewModel GetOrderCheckoutViewModel();
        Task<ProfileViewModel> GetProfileViewModelAsync();
        RegisterViewModel GetRegisterViewModel(RegisterUserData registerUserData);
        Task<TicketEditViewModel> GetTicketEditViewModelAsync(Ticket ticket);
        Task<TicketsViewModel> GetTicketsViewModelAsync(TicketIndexModel ticketIndexModel);
    }
}
