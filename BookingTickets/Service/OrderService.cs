﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookingTickets.Converter;
using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using BookingTickets.Extension;
using BookingTickets.Model;
using BookingTickets.Storage;
using BookingTickets.Validation;
using Microsoft.AspNetCore.Http;

namespace BookingTickets.Service
{
    public class OrderService : IOrderService
    {
        private readonly ICartStorage _cartStorage;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<Ticket> _ticketRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ITicketAmountCalculator _ticketAmountCalculator;
        private readonly TicketConverter _ticketConverter;
        private readonly ICartItemValidator _cartItemValidator;

        public OrderService(
            ICartStorage cartStorage, 
            IRepository<Order> orderRepository, 
            IRepository<OrderItem> orderItemRepository,
            IHttpContextAccessor httpContextAccessor, 
            IRepository<Ticket> ticketRepository, 
            ITicketAmountCalculator ticketAmountCalculator,
            TicketConverter ticketConverter, 
            ICartItemValidator cartItemValidator
        )
        {
            _cartStorage = cartStorage ?? throw new ArgumentNullException(nameof(cartStorage));
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _orderItemRepository = orderItemRepository ?? throw new ArgumentNullException(nameof(orderItemRepository));
            _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
            _ticketRepository = ticketRepository ?? throw new ArgumentNullException(nameof(ticketRepository));
            _ticketAmountCalculator = ticketAmountCalculator 
                ?? throw new ArgumentNullException(nameof(ticketAmountCalculator));
            _ticketConverter = ticketConverter 
                ?? throw new ArgumentNullException(nameof(ticketConverter));
            _cartItemValidator = cartItemValidator ?? throw new ArgumentNullException(nameof(cartItemValidator));
        }

        public async Task<Order> CreateOrderAsync()
        {
            IReadOnlyList<CartItem> cartItems = _cartStorage.Get().CartItems;
            string userId = _httpContextAccessor.HttpContext.User.GetUserId();
            if (cartItems.Count < 1)
            {
                throw new Exception("Корзина пуста");
            }

            if (string.IsNullOrEmpty(userId))
            {
                throw new Exception("Нет пользователя");
            }

            ValidateCartItems(cartItems);

            Order order = new Order
            {
                UserId = Guid.Parse(userId),
                Number = Guid.NewGuid().ToString(),
                Id = Guid.NewGuid()
            };
            await _orderRepository.UpdateAsync(order);
            foreach (CartItem cartItem in cartItems)
            {
                Ticket ticket = await _ticketRepository.GetByIdAsync(cartItem.TicketId);
                OrderItem orderItem = new OrderItem
                {
                    Amount = _ticketAmountCalculator.Calculate(_ticketConverter.ConvertToModel(ticket), cartItem.Quantity),
                    OrderId = order.Id,
                    Quantity = cartItem.Quantity,
                    TicketId = cartItem.TicketId
                };
                await _orderItemRepository.UpdateAsync(orderItem);
                ticket.Quantity -= cartItem.Quantity;
                await _ticketRepository.UpdateAsync(ticket);
            }
            _cartStorage.Get().Clear();
            _cartStorage.Save();

            return order;
        }

        private void ValidateCartItems(IEnumerable<CartItem> cartItems)
        {
            foreach (CartItem cartItem in cartItems)
            {
                ValidateResult validateResult = _cartItemValidator.Validate(cartItem);
                if (!validateResult.Success)
                {
                    throw new Exception(validateResult.Message);
                }
            }
        }
    }
}
