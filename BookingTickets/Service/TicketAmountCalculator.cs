﻿using BookingTickets.Model;

namespace BookingTickets.Service
{
    public class TicketAmountCalculator : ITicketAmountCalculator
    {
        public decimal Calculate(TicketModel ticket, int quantity) => ticket.Price * quantity;
    }
}
