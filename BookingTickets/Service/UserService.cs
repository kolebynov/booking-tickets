﻿using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using BookingTickets.Converter;
using BookingTickets.EFCore.Domain;
using BookingTickets.Extension;
using BookingTickets.Model;
using Microsoft.AspNetCore.Identity;

namespace BookingTickets.Service
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RegisterUserDataConverter _registerDataConverter;
        private readonly SignInResultConverter _signInResultConverter;
        private readonly IdentityResultConverter _identityResultConverter;

        public UserService(UserManager<User> userManager, 
            SignInManager<User> signInManager,
            RegisterUserDataConverter registerDataConverter,
            SignInResultConverter signInResultConverter,
            IdentityResultConverter identityResultConverter
        )
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _registerDataConverter = registerDataConverter ?? throw new ArgumentNullException(nameof(registerDataConverter));
            _signInResultConverter = signInResultConverter ?? throw new ArgumentNullException(nameof(signInResultConverter));
            _identityResultConverter = identityResultConverter ?? throw new ArgumentNullException(nameof(identityResultConverter));
        }

        public async Task<UserResult> LoginAsync(LoginData loginData)
        {
            User user = await _userManager.FindByEmailAsync(loginData.Email);
            UserResult loginResult = new UserResult { Success = false };
            if (user != null)
            {
                await _signInManager.SignOutAsync();
                SignInResult signInResult = await _signInManager.PasswordSignInAsync(user, loginData.Password, false, false);
                loginResult = _signInResultConverter.ConvertToUserResult(signInResult);
            }

            return loginResult;
        }

        public async Task LogoutAsync()
        {
            await _signInManager.SignOutAsync();
            _signInManager.Context.Session.Clear();
        }

        public async Task<UserResult> RegisterNewUserAsync(RegisterUserData registerUserData)
        {
            User newUser = _registerDataConverter.ConvertToUser(registerUserData);
            IdentityResult identityResult = await _userManager.CreateAsync(newUser, registerUserData.Password);
            await _userManager.AddToRoleAsync(newUser, RoleConstants.USER_ROLE_NAME);
            return _identityResultConverter.ConvertToUserResult(identityResult);
        }

        public async Task<bool> CurrentUserIsInRoleAsync(string role)
        {
            string userId = _signInManager.Context.User?.GetUserId();
            if (userId != null)
            {
                User user = new User { Id = new Guid(userId) };
                return await _userManager.IsInRoleAsync(user, role);
            }

            return false;
        }

        public async Task<User> GetUserAsync(ClaimsPrincipal principal)
        {
            return await _userManager.GetUserAsync(_signInManager.Context.User);
        }
    }
}