﻿using BookingTickets.Converter;
using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using BookingTickets.Extension;
using BookingTickets.Model;
using BookingTickets.ModelFilter;
using BookingTickets.Provider;
using BookingTickets.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingTickets.Service
{
    public class ViewModelsService : IViewModelsService
    {
        private readonly ICartService _cartService;
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserConverter _userConverter;
        private readonly IRepository<TicketType> _ticketTypeRepository;
        private readonly TicketConverter _ticketConverter;
        private readonly TicketTypeConverter _ticketTypeConverter;
        private readonly TicketIndexModelConverter _ticketIndexModelConverter;
        private readonly IFilter<Ticket, TicketSearchModel> _ticketFilter;
        private readonly ITicketProvider _ticketProvider;
        private readonly IItemsPerPagesProvider _itemsPerPagesProvider;

        public ViewModelsService(
            ICartService cartService,
            IUserService userService, 
            IHttpContextAccessor httpContextAccessor, 
            UserConverter userConverter, 
            IRepository<TicketType> ticketTypeRepository, 
            TicketConverter ticketConverter, 
            TicketTypeConverter ticketTypeConverter, 
            TicketIndexModelConverter ticketIndexModelConverter, 
            IFilter<Ticket, TicketSearchModel> ticketFilter, 
            ITicketProvider ticketProvider, 
            IItemsPerPagesProvider itemsPerPagesProvider
        )
        {
            _cartService = cartService ?? throw new ArgumentNullException(nameof(cartService));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
            _userConverter = userConverter ?? throw new ArgumentNullException(nameof(userConverter));
            _ticketTypeRepository = ticketTypeRepository ?? throw new ArgumentNullException(nameof(ticketTypeRepository));
            _ticketConverter = ticketConverter ?? throw new ArgumentNullException(nameof(ticketConverter));
            _ticketTypeConverter = ticketTypeConverter ?? throw new ArgumentNullException(nameof(ticketTypeConverter));
            _ticketIndexModelConverter = ticketIndexModelConverter 
                ?? throw new ArgumentNullException(nameof(ticketIndexModelConverter));
            _ticketFilter = ticketFilter ?? throw new ArgumentNullException(nameof(ticketFilter));
            _ticketProvider = ticketProvider ?? throw new ArgumentNullException(nameof(ticketProvider));
            _itemsPerPagesProvider = itemsPerPagesProvider 
                ?? throw new ArgumentNullException(nameof(itemsPerPagesProvider));
        }

        public CartViewModel GetCartViewModel(string returnUrl)
        {
            return new CartViewModel
            {
                CartItems = _cartService.GetItems(),
                ReturnUrl = returnUrl ?? "/"
            };
        }

        public LoginViewModel GetLoginViewModel(LoginData loginData)
        {
            return new LoginViewModel
            {
                LoginData = loginData
            };
        }

        public OrderCheckoutViewModel GetOrderCheckoutViewModel()
        {
            return new OrderCheckoutViewModel
            {
                CartItems = _cartService.GetItems()
            };
        }

        public async Task<ProfileViewModel> GetProfileViewModelAsync()
        {
            User user = await _userService.GetUserAsync(_httpContextAccessor.HttpContext.User);
            return new ProfileViewModel
            {
                ProfileInfo = _userConverter.ConvertToProfileInfo(user)
            };
        }

        public RegisterViewModel GetRegisterViewModel(RegisterUserData registerUserData)
        {
            return new RegisterViewModel
            {
                RegisterData = registerUserData
            };
        }

        public async Task<TicketEditViewModel> GetTicketEditViewModelAsync(Ticket ticket)
        {
            return new TicketEditViewModel
            {
                Ticket = _ticketConverter.ConvertToModel(ticket),
                TicketTypes = _ticketTypeRepository.Entities.Select(t => _ticketTypeConverter.ConvertToModel(t)).ToList(),
                CanEdit = await _userService.CurrentUserIsInRoleAsync(RoleConstants.ADMIN_ROLE_NAME)
            };
        }

        public async Task<TicketsViewModel> GetTicketsViewModelAsync(TicketIndexModel ticketIndexModel)
        {
            TicketSearchModel ticketSearchModel = _ticketIndexModelConverter.ConvertToTicketSearchModel(ticketIndexModel);
            IQueryable<Ticket> query = _ticketFilter.ApplyFilter(_ticketProvider.Get(), ticketSearchModel);
            IReadOnlyList<NameValuePair<string>> itemsPerPages = _itemsPerPagesProvider.Get();

            int page = ticketIndexModel.Page > 0 ? ticketIndexModel.Page : 1;
            int itemsPerPage = 0;
            if (ticketIndexModel.Count != _itemsPerPagesProvider.GetAllItemsParamName())
            {
                if (!int.TryParse(ticketIndexModel.Count, out itemsPerPage) || itemsPerPage < 1)
                {
                    int.TryParse(itemsPerPages[0].Value, out itemsPerPage);
                }
            }

            int totalItems = query.Count();
            if (itemsPerPage == 0)
            {
                itemsPerPage = totalItems;
                page = 1;
            }

            return new TicketsViewModel
            {
                Tickets = query
                    .Skip((page - 1) * itemsPerPage)
                    .Take(itemsPerPage)
                    .AsEnumerable()
                    .Select(t => _ticketConverter.ConvertToModel(t)),
                PaginationData = new PaginationData
                {
                    CurrentPage = page,
                    ItemsPerPage = itemsPerPage,
                    TotalItems = totalItems
                },
                ItemsPerPages = itemsPerPages,
                TicketTypes = _ticketTypeRepository.Entities
                    .AsEnumerable()
                    .Select(t => _ticketTypeConverter.ConvertToModel(t)),
                TicketIndexModel = ticketIndexModel,
                ReturnUrlToHome = _httpContextAccessor.HttpContext.Request.PathAndQuery(),
                SelectedItemsPerPage = ticketIndexModel.Count,
                CanEditTickets = await _userService.CurrentUserIsInRoleAsync(RoleConstants.ADMIN_ROLE_NAME)
            };
        }
    }
}