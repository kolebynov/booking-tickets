﻿using BookingTickets.Converter;
using BookingTickets.EFCore.Data;
using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using BookingTickets.Fake.Repository;
using BookingTickets.Model;
using BookingTickets.Provider;
using BookingTickets.Service;
using BookingTickets.Storage;
using BookingTickets.ViewModel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using BookingTickets.ModelFilter;
using BookingTickets.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;

namespace BookingTickets
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TicketContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("MainConnection"), 
                x => x.MigrationsAssembly("BookingTickets")));

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<TicketContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.Expiration = TimeSpan.FromDays(150);
                options.LoginPath = "/Account/Login";
                options.LogoutPath = "/Account/Logout";
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            //services.AddScoped(typeof(IRepository<>), typeof(EFRepository<>));
            //Fake repos
            services.AddSingleton<IRepository<Ticket>, FakeTicketRepository>();
            services.AddSingleton<IRepository<TicketType>, FakeTicketTypeRepository>();
            services.AddSingleton(typeof(IRepository<>), typeof(BaseFakeRepository<>));

            services.AddScoped<IViewModelsService, ViewModelsService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICartService, CartService>();
            services.AddScoped<ICartStorage, SessionCartStorage>();
            services.AddScoped<ITicketProvider, TicketProvider>();
            services.AddScoped<IItemsPerPagesProvider, ItemsPerPagesProvider>();
            services.AddScoped<ICartItemValidator, CartItemValidator>();
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IHttpContextAccessor>().HttpContext.Session);
            services.AddScoped<CartItemConverter, CartItemConverter>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddSingleton<TicketConverter, TicketConverter>();
            services.AddSingleton<RegisterUserDataConverter, RegisterUserDataConverter>();
            services.AddSingleton<IdentityResultConverter, IdentityResultConverter>();
            services.AddSingleton<SignInResultConverter, SignInResultConverter>();
            services.AddSingleton<TicketTypeConverter, TicketTypeConverter>();
            services.AddSingleton<UserConverter, UserConverter>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IFilter<Ticket, TicketSearchModel>, TicketFilter>();
            services.AddSingleton<TicketIndexModelConverter, TicketIndexModelConverter>();
            services.AddSingleton<ITicketAmountCalculator, TicketAmountCalculator>();

            services.AddMemoryCache();
            services.AddSession();

            services.AddMvc(config =>
            {
                AuthorizationPolicy policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .RequireRole(RoleConstants.USER_ROLE_NAME)
                    .Build();

                config.Filters.Add(new AuthorizeFilter(policy));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseSession();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Ticket}/{action=Index}");
            });
        }
    }
}
