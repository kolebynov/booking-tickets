﻿using BookingTickets.Model;

namespace BookingTickets.Storage
{
    public interface ICartStorage
    {
        Cart Get();
        void Save();
    }
}
