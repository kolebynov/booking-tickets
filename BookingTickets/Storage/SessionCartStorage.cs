﻿using BookingTickets.Extension;
using BookingTickets.Model;
using Microsoft.AspNetCore.Http;
using System;

namespace BookingTickets.Storage
{
    public class SessionCartStorage : ISessionCartStorage, IDisposable
    {
        private Cart _cart;
        private StorageState _state = StorageState.NotChanged;

        private readonly ISession _session;
        private readonly object _lockObj = new object();

        private static readonly string CART_KEY = "Cart";

        public SessionCartStorage(ISession session)
        {
            _session = session 
                ?? throw new ArgumentNullException(nameof(session));
        }

        public Cart Get()
        {
            if (_cart == null)
            {
                lock (_lockObj)
                {
                    if (_cart == null)
                    {
                        _cart = _session.GetJson<Cart>(CART_KEY) ?? new Cart();
                        _cart.Changed += (target, e) => _state = StorageState.Changed;
                    }
                }
                
            }

            return _cart;
        }

        public void Save()
        {
            if (_cart != null && _state != StorageState.NotChanged)
            {
                _session.SetJson(CART_KEY, _cart);
                _state = StorageState.NotChanged;
            }
        }

        public void Dispose() => Save();

        private enum StorageState
        {
            NotChanged,
            Changed
        }
    }
}
