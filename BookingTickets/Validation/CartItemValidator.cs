﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using BookingTickets.Model;

namespace BookingTickets.Validation
{
    public class CartItemValidator : ICartItemValidator
    {
        private readonly IRepository<Ticket> _ticketRepository;

        private static readonly string TICKET_NOT_FOUND = "Билет с id {0} не найден.";
        private static readonly string WRONG_QUANTITY = "Для билета {0} указано недопустимое количество (доступно {1}).";

        public CartItemValidator(IRepository<Ticket> ticketRepository)
        {
            _ticketRepository = ticketRepository ?? throw new ArgumentNullException(nameof(ticketRepository));
        }

        public ValidateResult Validate(CartItem cartItem)
        {
            ValidateResult validateResult = new ValidateResult();
            Ticket ticket = _ticketRepository.GetByIdAsync(cartItem.TicketId).Result;
            if (ticket == null)
            {
                validateResult.Message = string.Format(TICKET_NOT_FOUND, cartItem.TicketId);
            }
            else if (ticket.Quantity < cartItem.Quantity)
            {
                validateResult.Message = string.Format(WRONG_QUANTITY, ticket.Description, ticket.Quantity);
            }

            return validateResult;
        }
    }
}
