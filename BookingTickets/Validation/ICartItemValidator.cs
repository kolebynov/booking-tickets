﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.Model;

namespace BookingTickets.Validation
{
    public interface ICartItemValidator
    {
        ValidateResult Validate(CartItem cartItem);
    }
}
