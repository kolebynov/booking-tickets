﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.EFCore.Domain;
using BookingTickets.EFCore.Repository;
using BookingTickets.Model;

namespace BookingTickets.Validation
{
    public class UpdateCartItemsModelValidatorAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is UpdateCartItemsModel updateCartItemsModel)
            {
                ICartItemValidator cartItemValidator =
                    (ICartItemValidator) validationContext.GetService(typeof(ICartItemValidator));
                foreach (CartItem cartItem in updateCartItemsModel.Items)
                {
                    ValidateResult validateResult = cartItemValidator.Validate(cartItem);
                    if (!validateResult.Success)
                    {
                        return new ValidationResult(validateResult.Message, new[] { cartItem.TicketId.ToString() });
                    }
                }
            }

            return ValidationResult.Success;
        }
    }
}
