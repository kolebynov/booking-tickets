﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingTickets.Validation
{
    public class ValidateResult
    {
        public bool Success => Message == null;
        public string Message { get; set; }
    }
}
