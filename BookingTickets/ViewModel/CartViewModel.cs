﻿using System.Collections.Generic;
using BookingTickets.Model;

namespace BookingTickets.ViewModel
{
    public class CartViewModel
    {
        public IReadOnlyList<CartItemModel> CartItems { get; set; }
        public string ReturnUrl { get; set; }
    }
}
