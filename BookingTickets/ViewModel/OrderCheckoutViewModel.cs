﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.Model;

namespace BookingTickets.ViewModel
{
    public class OrderCheckoutViewModel
    {
        public IReadOnlyList<CartItemModel> CartItems { get; set; }
    }
}