﻿using BookingTickets.Model;

namespace BookingTickets.ViewModel
{
    public class ProfileViewModel
    {
        public ProfileInfoModel ProfileInfo { get; set; }
    }
}
