﻿using BookingTickets.Model;

namespace BookingTickets.ViewModel
{
    public class RegisterViewModel
    {
        public RegisterUserData RegisterData { get; set; }
    }
}
