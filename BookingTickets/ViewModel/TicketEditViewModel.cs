﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using BookingTickets.EFCore.Domain;
using BookingTickets.Model;

namespace BookingTickets.ViewModel
{
    public class TicketEditViewModel
    {
        public TicketModel Ticket { get; set; }
        public IReadOnlyList<TicketTypeModel> TicketTypes { get; set; }
        public bool CanEdit { get; set; }
    }
}
