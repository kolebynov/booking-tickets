﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingTickets.Model;

namespace BookingTickets.ViewModel
{
    public class TicketViewModel
    {
        public TicketModel Ticket { get; set; }
        public bool CanEdit { get; set; }
        public string ReturnUrl { get; set; }
    }
}
