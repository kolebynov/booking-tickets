﻿using BookingTickets.Model;
using System.Collections.Generic;

namespace BookingTickets.ViewModel
{
    public class TicketsViewModel
    {
        public IEnumerable<TicketModel> Tickets { get; set; }
        public PaginationData PaginationData { get; set; }
        public IEnumerable<NameValuePair<string>> ItemsPerPages { get; set; }
        public string SelectedItemsPerPage { get; set; }
        public IEnumerable<TicketTypeModel> TicketTypes { get; set; }
        public TicketIndexModel TicketIndexModel { get; set; }
        public string ReturnUrlToHome { get; set; }
        public bool CanEditTickets { get; set; }
    }
}
